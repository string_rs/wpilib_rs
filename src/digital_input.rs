use crate::*;

pub type DigitalInput = frc_DigitalInput;
impl DigitalInput {
    pub fn from(channel: i32) -> Self {
        unsafe {
            return DigitalInput::new(channel);
        }
    }

    pub fn get(&self) -> bool {
        unsafe {
            return self.Get();
        }
    }

    pub fn set_sim_device(&mut self, device: HAL_SimDeviceHandle) {
        unsafe {
            self.SetSimDevice(device);
        }
    }

    pub fn get_port_handle_for_routing(&mut self) -> HAL_Handle {
        unsafe {
            return frc_DigitalInput_GetPortHandleForRouting(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_analog_trigger_type_for_routing(&mut self) -> AnalogTriggerType {
        unsafe {
            return frc_DigitalInput_GetAnalogTriggerTypeForRouting(self as *mut _ as *mut c_void);
        }
    }

    pub fn is_analog_trigger(&mut self) -> bool {
        unsafe {
            return frc_DigitalInput_IsAnalogTrigger(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_channel(&mut self) -> i32 {
        unsafe {
            return frc_DigitalInput_GetChannel(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_DigitalInput_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
