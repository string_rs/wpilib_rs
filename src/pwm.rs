use crate::*;

pub type PeriodMultiplier = frc_PWM_PeriodMultiplier;
pub type PWM = frc_PWM;
impl PWM {
    pub fn from(channel: i32, register_sendable: bool) -> Self {
        unsafe {
            return PWM::new(channel, register_sendable);
        }
    }

    pub fn set_period_multiplier(&mut self, mult: PeriodMultiplier) {
        unsafe {
            self.SetPeriodMultiplier(mult);
        }
    }

    pub fn set_zero_latch(&mut self) {
        unsafe {
            self.SetZeroLatch();
        }
    }

    pub fn enable_deadband_elimination(&mut self, eliminate_deadband: bool) {
        unsafe {
            self.EnableDeadbandElimination(eliminate_deadband);
        }
    }

    pub fn set_bounds(
        &mut self,
        max: f64,
        deadband_max: f64,
        center: f64,
        deadband_min: f64,
        min: f64,
    ) {
        unsafe {
            self.SetBounds(max, deadband_max, center, deadband_min, min);
        }
    }

    pub fn set_raw_bounds(
        &mut self,
        max: i32,
        deadband_max: i32,
        center: i32,
        deadband_min: i32,
        min: i32,
    ) {
        unsafe {
            self.SetRawBounds(max, deadband_max, center, deadband_min, min);
        }
    }

    pub fn get_raw_bounds(
        &mut self,
        max: &mut i32,
        deadband_max: &mut i32,
        center: &mut i32,
        deadband_min: &mut i32,
        min: &mut i32,
    ) {
        unsafe {
            self.GetRawBounds(max, deadband_max, center, deadband_min, min);
        }
    }

    pub fn get_channel(&mut self) -> i32 {
        unsafe {
            return self.GetChannel();
        }
    }

    pub fn set_raw(&mut self, value: u16) {
        unsafe {
            frc_PWM_SetRaw(self as *mut _ as *mut c_void, value);
        }
    }

    pub fn get_raw(&mut self) -> u16 {
        unsafe {
            return frc_PWM_GetRaw(self as *mut _ as *mut c_void);
        }
    }

    pub fn set_position(&mut self, pos: f64) {
        unsafe {
            frc_PWM_SetPosition(self as *mut _ as *mut c_void, pos);
        }
    }

    pub fn get_position(&mut self) -> f64 {
        unsafe {
            return frc_PWM_GetPosition(self as *mut _ as *mut c_void);
        }
    }

    pub fn set_speed(&mut self, speed: f64) {
        unsafe {
            frc_PWM_SetSpeed(self as *mut _ as *mut c_void, speed);
        }
    }

    pub fn get_speed(&mut self) -> f64 {
        unsafe {
            return frc_PWM_GetSpeed(self as *mut _ as *mut c_void);
        }
    }

    pub fn set_disabled(&mut self) {
        unsafe {
            frc_PWM_SetDisabled(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_PWM_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
