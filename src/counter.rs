use crate::*;

pub type EncodingType = frc_CounterBase_EncodingType;
pub type CounterMode = frc_Counter_Mode;
pub type Counter = frc_Counter;
impl Counter {
    pub fn from(mode: CounterMode) -> Self {
        unsafe {
            return Counter::new(mode);
        }
    }

    pub fn from_channel(channel: i32) -> Self {
        unsafe {
            return Counter::new1(channel);
        }
    }

    pub fn from_source(source: &mut DigitalSource) -> Self {
        unsafe {
            return Counter::new2(source);
        }
    }

    pub fn from_trigger(trigger: &AnalogTrigger) -> Self {
        unsafe {
            return Counter::new4(trigger);
        }
    }

    pub fn from_sources(
        encoding_type: EncodingType,
        up_source: &mut DigitalSource,
        down_source: &mut DigitalSource,
        inverted: bool,
    ) {
        unsafe {
            Counter::new5(encoding_type, up_source, down_source, inverted);
        }
    }

    pub fn set_up_source_channel(&mut self, channel: i32) {
        unsafe {
            self.SetUpSource(channel);
        }
    }

    pub fn set_up_source_trigger(
        &mut self,
        trigger: &mut AnalogTrigger,
        trigger_type: AnalogTriggerType,
    ) {
        unsafe {
            self.SetUpSource1(trigger, trigger_type);
        }
    }

    pub fn set_up_source_digital(&mut self, source: &mut DigitalSource) {
        unsafe {
            self.SetUpSource3(source);
        }
    }

    pub fn set_up_source_edge(&mut self, rising_edge: bool, falling_edge: bool) {
        unsafe {
            self.SetUpSourceEdge(rising_edge, falling_edge);
        }
    }

    pub fn clear_up_source(&mut self) {
        unsafe {
            self.ClearUpSource();
        }
    }

    pub fn set_down_source_channel(&mut self, channel: i32) {
        unsafe {
            self.SetDownSource(channel);
        }
    }

    pub fn set_down_source_trigger(
        &mut self,
        trigger: &mut AnalogTrigger,
        trigger_type: AnalogTriggerType,
    ) {
        unsafe {
            self.SetDownSource1(trigger, trigger_type);
        }
    }

    pub fn set_down_source_digital(&mut self, source: &mut DigitalSource) {
        unsafe {
            self.SetDownSource3(source);
        }
    }

    pub fn set_down_source_edge(&mut self, rising_edge: bool, falling_edge: bool) {
        unsafe {
            self.SetDownSourceEdge(rising_edge, falling_edge);
        }
    }

    pub fn clear_down_source(&mut self) {
        unsafe {
            self.ClearDownSource();
        }
    }

    pub fn set_up_down_counter_mode(&mut self) {
        unsafe {
            self.SetUpDownCounterMode();
        }
    }

    pub fn set_external_direction_mode(&mut self) {
        unsafe {
            self.SetExternalDirectionMode();
        }
    }

    pub fn set_semi_period_mode(&mut self, high_semi_period: bool) {
        unsafe {
            self.SetSemiPeriodMode(high_semi_period);
        }
    }

    pub fn set_pulse_length_mode(&mut self, threshold: f64) {
        unsafe {
            self.SetPulseLengthMode(threshold);
        }
    }

    pub fn set_reverse_direction(&mut self, reverse_direction: bool) {
        unsafe {
            self.SetReverseDirection(reverse_direction);
        }
    }

    pub fn set_samples_to_average(&mut self, samples_to_average: i32) {
        unsafe {
            self.SetSamplesToAverage(samples_to_average);
        }
    }

    pub fn get_samples_to_average(&self) -> i32 {
        unsafe {
            return self.GetSamplesToAverage();
        }
    }

    pub fn get_fpga_index(&self) -> i32 {
        unsafe {
            return self.GetFPGAIndex();
        }
    }

    pub fn set_update_when_empty(&mut self, enabled: bool) {
        unsafe {
            self.SetUpdateWhenEmpty(enabled);
        }
    }

    pub fn get(&mut self) -> i32 {
        unsafe {
            return frc_Counter_Get(self as *mut _ as *mut c_void);
        }
    }

    pub fn reset(&mut self) {
        unsafe {
            frc_Counter_Reset(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_period(&mut self) -> units_time_second_t {
        unsafe {
            return frc_Counter_GetPeriod(self as *mut _ as *mut c_void);
        }
    }

    pub fn set_max_period(&mut self, max_period: units_time_second_t) {
        unsafe {
            frc_Counter_SetMaxPeriod(self as *mut _ as *mut c_void, max_period);
        }
    }

    pub fn get_stopped(&mut self) -> bool {
        unsafe {
            return frc_Counter_GetStopped(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_direction(&mut self) -> bool {
        unsafe {
            return frc_Counter_GetDirection(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_Counter_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
