use crate::*;

pub type IterativeRobotBase = frc_IterativeRobotBase;
impl IterativeRobotBase {
    pub fn from(period: f64) -> Self {
        unsafe {
            return IterativeRobotBase::new(period);
        }
    }

    pub fn from_seconds(period: units_time_second_t) -> Self {
        unsafe {
            return IterativeRobotBase::new1(period);
        }
    }

    pub fn set_network_tables_flush_enabled(&mut self, enabled: bool) {
        unsafe {
            self.SetNetworkTablesFlushEnabled(enabled);
        }
    }

    pub fn get_period(&self) -> units_time_second_t {
        unsafe {
            return self.GetPeriod();
        }
    }

    pub fn loop_func(&mut self) {
        unsafe {
            self.LoopFunc();
        }
    }

    pub fn robot_init(&mut self) {
        unsafe {
            frc_IterativeRobotBase_RobotInit(self as *mut _ as *mut c_void);
        }
    }

    pub fn simulation_init(&mut self) {
        unsafe {
            frc_IterativeRobotBase_SimulationInit(self as *mut _ as *mut c_void);
        }
    }

    pub fn disabled_init(&mut self) {
        unsafe {
            frc_IterativeRobotBase_DisabledInit(self as *mut _ as *mut c_void);
        }
    }

    pub fn autonomous_init(&mut self) {
        unsafe {
            frc_IterativeRobotBase_AutonomousInit(self as *mut _ as *mut c_void);
        }
    }

    pub fn teleop_init(&mut self) {
        unsafe {
            frc_IterativeRobotBase_TeleopInit(self as *mut _ as *mut c_void);
        }
    }

    pub fn test_init(&mut self) {
        unsafe {
            frc_IterativeRobotBase_TestInit(self as *mut _ as *mut c_void);
        }
    }

    pub fn robot_periodic(&mut self) {
        unsafe {
            frc_IterativeRobotBase_RobotPeriodic(self as *mut _ as *mut c_void);
        }
    }

    pub fn simulation_periodic(&mut self) {
        unsafe {
            frc_IterativeRobotBase_SimulationPeriodic(self as *mut _ as *mut c_void);
        }
    }

    pub fn disabled_periodic(&mut self) {
        unsafe {
            frc_IterativeRobotBase_DisabledPeriodic(self as *mut _ as *mut c_void);
        }
    }

    pub fn autonomous_periodic(&mut self) {
        unsafe {
            frc_IterativeRobotBase_AutonomousPeriodic(self as *mut _ as *mut c_void);
        }
    }

    pub fn teleop_periodic(&mut self) {
        unsafe {
            frc_IterativeRobotBase_TeleopPeriodic(self as *mut _ as *mut c_void);
        }
    }

    pub fn test_periodic(&mut self) {
        unsafe {
            frc_IterativeRobotBase_TestPeriodic(self as *mut _ as *mut c_void);
        }
    }

    pub fn disabled_exit(&mut self) {
        unsafe {
            frc_IterativeRobotBase_DisabledExit(self as *mut _ as *mut c_void);
        }
    }

    pub fn autonomous_exit(&mut self) {
        unsafe {
            frc_IterativeRobotBase_AutonomousExit(self as *mut _ as *mut c_void);
        }
    }

    pub fn teleop_exit(&mut self) {
        unsafe {
            frc_IterativeRobotBase_TeleopExit(self as *mut _ as *mut c_void);
        }
    }

    pub fn test_exit(&mut self) {
        unsafe {
            frc_IterativeRobotBase_TestExit(self as *mut _ as *mut c_void);
        }
    }
}
