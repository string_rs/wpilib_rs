use crate::*;

pub type AnalogTriggerOutput = frc_AnalogTriggerOutput;
impl AnalogTriggerOutput {
    pub fn from(trigger: &AnalogTrigger, output_type: AnalogTriggerType) -> Self {
        unsafe {
            return AnalogTriggerOutput::new(trigger, output_type);
        }
    }

    pub fn get(&self) -> bool {
        unsafe {
            return self.Get();
        }
    }

    pub fn get_port_handle_for_routing(&mut self) -> HAL_Handle {
        unsafe {
            return frc_AnalogTriggerOutput_GetPortHandleForRouting(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_analog_trigger_type_for_routing(&mut self) -> AnalogTriggerType {
        unsafe {
            return frc_AnalogTriggerOutput_GetAnalogTriggerTypeForRouting(
                self as *mut _ as *mut c_void,
            );
        }
    }

    pub fn is_analog_trigger(&mut self) -> bool {
        unsafe {
            return frc_AnalogTriggerOutput_IsAnalogTrigger(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_channel(&mut self) -> i32 {
        unsafe {
            return frc_AnalogTriggerOutput_GetChannel(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_AnalogTriggerOutput_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
