use crate::*;

pub type DriverStation = frc_DriverStation;
impl DriverStation {
    pub fn get_instance() -> &'static mut Self {
        unsafe {
            return DriverStation::GetInstance().as_mut().unwrap();
        }
    }

    pub fn get_stick_button(stick: i32, button: i32) -> bool {
        unsafe {
            return DriverStation::GetStickButton(stick, button);
        }
    }

    pub fn get_stick_button_pressed(stick: i32, button: i32) -> bool {
        unsafe {
            return DriverStation::GetStickButtonPressed(stick, button);
        }
    }

    pub fn get_stick_button_released(stick: i32, button: i32) -> bool {
        unsafe {
            return DriverStation::GetStickButtonReleased(stick, button);
        }
    }

    pub fn get_stick_axis(stick: i32, axis: i32) -> f64 {
        unsafe {
            return DriverStation::GetStickAxis(stick, axis);
        }
    }

    pub fn get_stick_pov(stick: i32, pov: i32) -> i32 {
        unsafe {
            return DriverStation::GetStickPOV(stick, pov);
        }
    }

    pub fn get_stick_buttons(stick: i32) -> i32 {
        unsafe {
            return DriverStation::GetStickButtons(stick);
        }
    }

    pub fn get_stick_axis_count(stick: i32) -> i32 {
        unsafe {
            return DriverStation::GetStickAxisCount(stick);
        }
    }

    pub fn get_stick_pov_count(stick: i32) -> i32 {
        unsafe {
            return DriverStation::GetStickPOVCount(stick);
        }
    }

    pub fn get_stick_button_count(stick: i32) -> i32 {
        unsafe {
            return DriverStation::GetStickButtonCount(stick);
        }
    }

    pub fn get_joystick_is_xbox(stick: i32) -> bool {
        unsafe {
            return DriverStation::GetJoystickIsXbox(stick);
        }
    }

    pub fn get_joystick_type(stick: i32) -> i32 {
        unsafe {
            return DriverStation::GetJoystickType(stick);
        }
    }

    // TODO: Convert to Rust string instead
    pub fn get_joystick_name(stick: i32) -> std_string {
        unsafe {
            return DriverStation::GetJoystickName(stick);
        }
    }

    pub fn get_joystick_axis_type(stick: i32, axis: i32) -> i32 {
        unsafe {
            return DriverStation::GetJoystickAxisType(stick, axis);
        }
    }

    pub fn is_joystick_connected(stick: i32) -> bool {
        unsafe {
            return DriverStation::IsJoystickConnected(stick);
        }
    }

    pub fn is_enabled() -> bool {
        unsafe {
            return DriverStation::IsEnabled();
        }
    }

    pub fn is_disabled() -> bool {
        unsafe {
            return DriverStation::IsDisabled();
        }
    }

    pub fn is_e_stopped() -> bool {
        unsafe {
            return DriverStation::IsEStopped();
        }
    }

    pub fn is_autonomous() -> bool {
        unsafe {
            return DriverStation::IsAutonomous();
        }
    }

    pub fn is_autotomous_enabled() -> bool {
        unsafe {
            return DriverStation::IsAutonomousEnabled();
        }
    }

    pub fn is_operator_control() -> bool {
        unsafe {
            return DriverStation::IsOperatorControl();
        }
    }

    pub fn is_teleop_enabled() -> bool {
        unsafe {
            return DriverStation::IsTeleopEnabled();
        }
    }

    pub fn is_test() -> bool {
        unsafe {
            return DriverStation::IsTest();
        }
    }

    pub fn is_ds_attached() -> bool {
        unsafe {
            return DriverStation::IsDSAttached();
        }
    }

    pub fn is_fms_attached() -> bool {
        unsafe {
            return DriverStation::IsFMSAttached();
        }
    }

    // TODO: Convert to Rust string instead
    pub fn get_game_specific_message() -> std_string {
        unsafe {
            return DriverStation::GetGameSpecificMessage();
        }
    }
}
