use crate::*;

pub type AnalogOutput = frc_AnalogOutput;
impl AnalogOutput {
    pub fn from(channel: i32) -> Self {
        unsafe {
            return AnalogOutput::new(channel);
        }
    }

    pub fn set_voltage(&mut self, voltage: f64) {
        unsafe {
            self.SetVoltage(voltage);
        }
    }

    pub fn get_voltage(&self) -> f64 {
        unsafe {
            return self.GetVoltage();
        }
    }

    pub fn get_channel(&self) -> i32 {
        unsafe {
            return self.GetChannel();
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_AnalogOutput_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
