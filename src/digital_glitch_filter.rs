use crate::*;

pub type DigitalGlitchFilter = frc_DigitalGlitchFilter;
impl DigitalGlitchFilter {
    pub fn from() -> Self {
        unsafe {
            return DigitalGlitchFilter::new();
        }
    }

    pub fn add_source(&mut self, source: &mut DigitalSource) {
        unsafe {
            self.Add(source);
        }
    }

    pub fn add_input(&mut self, input: &mut Encoder) {
        unsafe {
            self.Add1(input);
        }
    }

    pub fn add_counter(&mut self, counter: &mut Counter) {
        unsafe {
            self.Add2(counter);
        }
    }

    pub fn remove_source(&mut self, source: &mut DigitalSource) {
        unsafe {
            self.Remove(source);
        }
    }

    pub fn remove_input(&mut self, input: &mut Encoder) {
        unsafe {
            self.Remove1(input);
        }
    }

    pub fn remove_counter(&mut self, counter: &mut Counter) {
        unsafe {
            self.Remove2(counter);
        }
    }

    pub fn set_period_cycles(&mut self, cycles: i32) {
        unsafe {
            self.SetPeriodCycles(cycles);
        }
    }

    pub fn set_period_nano_seconds(&mut self, nanoseconds: u64) {
        unsafe {
            self.SetPeriodNanoSeconds(nanoseconds);
        }
    }

    pub fn get_period_cycles(&mut self) -> i32 {
        unsafe {
            return self.GetPeriodCycles();
        }
    }

    pub fn get_period_nano_seconds(&mut self) -> u64 {
        unsafe {
            return self.GetPeriodNanoSeconds();
        }
    }
}
