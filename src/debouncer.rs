use crate::*;

pub type DebounceType = frc_Debouncer_DebounceType;
pub type Debouncer = frc_Debouncer;
impl Debouncer {
    pub fn from(debounce_time: units_time_second_t, type_: DebounceType) -> Self {
        unsafe {
            return Debouncer::new(debounce_time, type_);
        }
    }

    pub fn calculate(&mut self, input: bool) -> bool {
        unsafe {
            return self.Calculate(input);
        }
    }
}
