use crate::*;

pub type IndexingType = frc_Encoder_IndexingType;
pub type Encoder = frc_Encoder;
impl Encoder {
    pub fn from(
        a_channel: i32,
        b_channel: i32,
        reverse_direction: bool,
        encoding_type: EncodingType,
    ) -> Self {
        unsafe {
            return Encoder::new(a_channel, b_channel, reverse_direction, encoding_type);
        }
    }

    pub fn from_source(
        a_source: &mut DigitalSource,
        b_source: &mut DigitalSource,
        reverse_direction: bool,
        encoding_type: EncodingType,
    ) -> Self {
        unsafe {
            return Encoder::new1(a_source, b_source, reverse_direction, encoding_type);
        }
    }

    pub fn get_raw(&self) -> i32 {
        unsafe {
            return self.GetRaw();
        }
    }

    pub fn get_encoding_scale(&self) -> i32 {
        unsafe {
            return self.GetEncodingScale();
        }
    }

    pub fn get_distance(&self) -> f64 {
        unsafe {
            return self.GetDistance();
        }
    }

    pub fn get_rate(&self) -> f64 {
        unsafe {
            return self.GetRate();
        }
    }

    pub fn set_min_rate(&mut self, min_rate: f64) {
        unsafe {
            self.SetMinRate(min_rate);
        }
    }

    pub fn set_distance_per_pulse(&mut self, distance_per_pulse: f64) {
        unsafe {
            self.SetDistancePerPulse(distance_per_pulse);
        }
    }

    pub fn get_distance_per_pulse(&self) -> f64 {
        unsafe {
            return self.GetDistancePerPulse();
        }
    }

    pub fn set_reverse_direction(&mut self, reverse_direction: bool) {
        unsafe {
            self.SetReverseDirection(reverse_direction);
        }
    }

    pub fn set_samples_to_average(&mut self, samples_to_average: i32) {
        unsafe {
            self.SetSamplesToAverage(samples_to_average);
        }
    }

    pub fn get_samples_to_average(&self) -> i32 {
        unsafe {
            return self.GetSamplesToAverage();
        }
    }

    pub fn set_index_channel(&mut self, channel: i32, type_: IndexingType) {
        unsafe {
            self.SetIndexSource(channel, type_);
        }
    }

    pub fn set_index_source(&mut self, source: &mut DigitalSource, type_: IndexingType) {
        unsafe {
            self.SetIndexSource1(source, type_);
        }
    }

    pub fn set_sim_device(&mut self, device: HAL_SimDeviceHandle) {
        unsafe {
            self.SetSimDevice(device);
        }
    }

    pub fn get_fpga_index(&self) -> i32 {
        unsafe {
            return self.GetFPGAIndex();
        }
    }

    pub fn get(&mut self) -> i32 {
        unsafe {
            return frc_Encoder_Get(self as *mut _ as *mut c_void);
        }
    }

    pub fn reset(&mut self) {
        unsafe {
            frc_Encoder_Reset(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_period(&mut self) -> units_time_second_t {
        unsafe {
            return frc_Encoder_GetPeriod(self as *mut _ as *mut c_void);
        }
    }

    pub fn set_max_period(&mut self, max_period: units_time_second_t) {
        unsafe {
            frc_Encoder_SetMaxPeriod(self as *mut _ as *mut c_void, max_period);
        }
    }

    pub fn get_stopped(&mut self) -> bool {
        unsafe {
            return frc_Encoder_GetStopped(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_direction(&mut self) -> bool {
        unsafe {
            return frc_Encoder_GetDirection(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_Encoder_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
