use crate::*;

pub type Preferences = frc_Preferences;
impl Preferences {
    pub fn get_instance() -> &'static mut Self {
        unsafe {
            return Preferences::GetInstance().as_mut().unwrap();
        }
    }

    // TODO: Bindgen isn't working for this class.
}
