use crate::*;

pub type BuiltInAccelerometer = frc_BuiltInAccelerometer;
impl BuiltInAccelerometer {
    pub fn from(range: AccelerometerRange) -> Self {
        unsafe {
            return BuiltInAccelerometer::new(range);
        }
    }

    pub fn set_range(&mut self, range: AccelerometerRange) {
        unsafe {
            frc_BuiltInAccelerometer_SetRange(self as *mut _ as *mut c_void, range);
        }
    }

    pub fn get_x(&mut self) -> f64 {
        unsafe {
            return frc_BuiltInAccelerometer_GetX(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_y(&mut self) -> f64 {
        unsafe {
            return frc_BuiltInAccelerometer_GetY(self as *mut _ as *mut c_void);
        }
    }

    pub fn get_z(&mut self) -> f64 {
        unsafe {
            return frc_BuiltInAccelerometer_GetZ(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_BuiltInAccelerometer_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
