use crate::*;

pub type CANData = frc_CANData;
pub type CAN = frc_CAN;
impl CAN {
    pub fn from(device_id: i32) -> Self {
        unsafe {
            return CAN::new(device_id);
        }
    }

    pub fn from_preset(device_id: i32, device_manufacturer: i32, device_type: i32) -> Self {
        unsafe {
            return CAN::new1(device_id, device_manufacturer, device_type);
        }
    }

    pub fn write_packet(&mut self, data: u8, length: i32, api_id: i32) {
        unsafe {
            self.WritePacket(&data, length, api_id);
        }
    }

    pub fn write_packet_repeating(&mut self, data: u8, length: i32, api_id: i32, repeat_ms: i32) {
        unsafe {
            self.WritePacketRepeating(&data, length, api_id, repeat_ms);
        }
    }

    pub fn write_rtr_frame(&mut self, length: i32, api_id: i32) {
        unsafe {
            self.WriteRTRFrame(length, api_id);
        }
    }

    pub fn write_packet_no_error(&mut self, data: u8, length: i32, api_id: i32) -> i32 {
        unsafe {
            return self.WritePacketNoError(&data, length, api_id);
        }
    }

    pub fn write_packet_repeating_no_error(
        &mut self,
        data: u8,
        length: i32,
        api_id: i32,
        repeat_ms: i32,
    ) -> i32 {
        unsafe {
            return self.WritePacketRepeatingNoError(&data, length, api_id, repeat_ms);
        }
    }

    pub fn write_rtr_frame_no_error(&mut self, length: i32, api_id: i32) -> i32 {
        unsafe {
            return self.WriteRTRFrameNoError(length, api_id);
        }
    }

    pub fn stop_packet_repeating(&mut self, api_id: i32) {
        unsafe {
            self.StopPacketRepeating(api_id);
        }
    }

    pub fn read_packet_new(&mut self, api_id: i32, data: &mut CANData) -> bool {
        unsafe {
            return self.ReadPacketNew(api_id, data);
        }
    }

    pub fn read_packet_latest(&mut self, api_id: i32, data: &mut CANData) -> bool {
        unsafe {
            return self.ReadPacketLatest(api_id, data);
        }
    }

    pub fn read_packet_timeout(
        &mut self,
        api_id: i32,
        timeout_ms: i32,
        data: &mut CANData,
    ) -> bool {
        unsafe {
            return self.ReadPacketTimeout(api_id, timeout_ms, data);
        }
    }
}
