use crate::*;

pub type Joystick = frc_Joystick;
impl Joystick {
    pub fn from(port: i32) -> Self {
        unsafe {
            return Joystick::new(port);
        }
    }

    pub fn set_x_channel(&mut self, channel: i32) {
        unsafe {
            self.SetXChannel(channel);
        }
    }

    pub fn set_y_channel(&mut self, channel: i32) {
        unsafe {
            self.SetYChannel(channel);
        }
    }

    pub fn set_z_channel(&mut self, channel: i32) {
        unsafe {
            self.SetZChannel(channel);
        }
    }

    pub fn set_twist_channel(&mut self, channel: i32) {
        unsafe {
            self.SetTwistChannel(channel);
        }
    }

    pub fn set_throttle_channel(&mut self, channel: i32) {
        unsafe {
            self.SetThrottleChannel(channel);
        }
    }

    pub fn get_x_channel(&self) -> i32 {
        unsafe {
            return self.GetXChannel();
        }
    }

    pub fn get_y_channel(&self) -> i32 {
        unsafe {
            return self.GetYChannel();
        }
    }

    pub fn get_z_channel(&self) -> i32 {
        unsafe {
            return self.GetZChannel();
        }
    }

    pub fn get_twist_channel(&self) -> i32 {
        unsafe {
            return self.GetTwistChannel();
        }
    }

    pub fn get_throttle_channel(&self) -> i32 {
        unsafe {
            return self.GetThrottleChannel();
        }
    }

    pub fn get_x(&self) -> f64 {
        unsafe {
            return self.GetX();
        }
    }

    pub fn get_y(&self) -> f64 {
        unsafe {
            return self.GetY();
        }
    }

    pub fn get_z(&self) -> f64 {
        unsafe {
            return self.GetZ();
        }
    }

    pub fn get_twist(&self) -> f64 {
        unsafe {
            return self.GetTwist();
        }
    }

    pub fn get_throttle(&self) -> f64 {
        unsafe {
            return self.GetThrottle();
        }
    }

    pub fn get_trigger(&self) -> bool {
        unsafe {
            return self.GetTrigger();
        }
    }

    pub fn get_trigger_pressed(&mut self) -> bool {
        unsafe {
            return self.GetTriggerPressed();
        }
    }

    pub fn get_trigger_released(&mut self) -> bool {
        unsafe {
            return self.GetTriggerReleased();
        }
    }

    pub fn get_top(&self) -> bool {
        unsafe {
            return self.GetTop();
        }
    }

    pub fn get_top_pressed(&mut self) -> bool {
        unsafe {
            return self.GetTopPressed();
        }
    }

    pub fn get_top_released(&mut self) -> bool {
        unsafe {
            return self.GetTopReleased();
        }
    }

    pub fn get_magnitude(&self) -> f64 {
        unsafe {
            return self.GetMagnitude();
        }
    }

    pub fn get_direction_radians(&self) -> f64 {
        unsafe {
            return self.GetDirectionRadians();
        }
    }

    pub fn get_direction_degrees(&self) -> f64 {
        unsafe {
            return self.GetDirectionDegrees();
        }
    }
}
