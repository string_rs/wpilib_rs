use crate::*;

pub type DSControlWord = frc_DSControlWord;
impl DSControlWord {
    pub fn from() -> Self {
        unsafe {
            return DSControlWord::new();
        }
    }

    pub fn is_enabled(&self) -> bool {
        unsafe {
            return self.IsEnabled();
        }
    }

    pub fn is_disabled(&self) -> bool {
        unsafe {
            return self.IsDisabled();
        }
    }

    pub fn is_e_stopped(&self) -> bool {
        unsafe {
            return self.IsEStopped();
        }
    }

    pub fn is_autonomous(&self) -> bool {
        unsafe {
            return self.IsAutonomous();
        }
    }

    pub fn is_autonomous_enabled(&self) -> bool {
        unsafe {
            return self.IsAutonomousEnabled();
        }
    }

    pub fn is_teleop(&self) -> bool {
        unsafe {
            return self.IsTeleop();
        }
    }

    pub fn is_teleop_enabled(&self) -> bool {
        unsafe {
            return self.IsTeleopEnabled();
        }
    }

    pub fn is_test(&self) -> bool {
        unsafe {
            return self.IsTest();
        }
    }

    pub fn is_ds_attached(&self) -> bool {
        unsafe {
            return self.IsDSAttached();
        }
    }

    pub fn is_fms_attached(&self) -> bool {
        unsafe {
            return self.IsFMSAttached();
        }
    }
}
