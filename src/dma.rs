use crate::*;

pub type DMA = frc_DMA;
impl DMA {
    pub fn from() -> Self {
        unsafe {
            return DMA::new();
        }
    }

    pub fn set_pause(&mut self, pause: bool) {
        unsafe {
            self.SetPause(pause);
        }
    }

    pub fn set_timed_trigger(&mut self, seconds: units_time_second_t) {
        unsafe {
            self.SetTimedTrigger(seconds);
        }
    }

    pub fn set_timed_trigger_cycles(&mut self, cycles: i32) {
        unsafe {
            self.SetTimedTriggerCycles(cycles);
        }
    }

    pub fn add_encoder(&mut self, encoder: &Encoder) {
        unsafe {
            self.AddEncoder(encoder);
        }
    }

    pub fn add_encoder_period(&mut self, encoder: &Encoder) {
        unsafe {
            self.AddEncoderPeriod(encoder);
        }
    }

    pub fn add_counter(&mut self, counter: &Counter) {
        unsafe {
            self.AddCounter(counter);
        }
    }

    pub fn add_counter_period(&mut self, counter: &Counter) {
        unsafe {
            self.AddCounterPeriod(counter);
        }
    }

    pub fn add_digital_source(&mut self, source: &DigitalSource) {
        unsafe {
            self.AddDigitalSource(source);
        }
    }

    pub fn add_duty_cycle(&mut self, duty_cycle: &DutyCycle) {
        unsafe {
            self.AddDutyCycle(duty_cycle);
        }
    }

    pub fn add_analog_input(&mut self, input: &AnalogInput) {
        unsafe {
            self.AddAnalogInput(input);
        }
    }

    pub fn add_averaged_analog_input(&mut self, input: &AnalogInput) {
        unsafe {
            self.AddAveragedAnalogInput(input);
        }
    }

    pub fn add_analog_accumulator(&mut self, accumulator: &AnalogInput) {
        unsafe {
            self.AddAnalogAccumulator(accumulator);
        }
    }

    pub fn set_external_trigger(
        &mut self,
        source: &mut DigitalSource,
        rising: bool,
        falling: bool,
    ) -> i32 {
        unsafe {
            return self.SetExternalTrigger(source, rising, falling);
        }
    }

    pub fn set_pwm_edge_trigger(&mut self, pwm: &mut PWM, rising: bool, falling: bool) -> i32 {
        unsafe {
            return self.SetPwmEdgeTrigger(pwm, rising, falling);
        }
    }

    pub fn set_pwm_edge_trigger_motor(
        &mut self,
        pwm: &mut PWMMotorController,
        rising: bool,
        falling: bool,
    ) {
        unsafe {
            self.SetPwmEdgeTrigger1(pwm, rising, falling);
        }
    }

    pub fn clear_sensors(&mut self) {
        unsafe {
            self.ClearSensors();
        }
    }

    pub fn clear_external_triggers(&mut self) {
        unsafe {
            self.ClearExternalTriggers();
        }
    }

    pub fn start(&mut self, queue_depth: i32) {
        unsafe {
            self.Start(queue_depth);
        }
    }

    pub fn stop(&mut self) {
        unsafe {
            self.Stop();
        }
    }
}
