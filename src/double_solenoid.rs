use crate::*;

pub type DoubleSolenoidValue = frc_DoubleSolenoid_Value;
pub type DoubleSolenoid = frc_DoubleSolenoid;
impl DoubleSolenoid {
    pub fn from(
        module: i32,
        module_type: PneumaticsModuleType,
        forward_channel: i32,
        reverse_channel: i32,
    ) -> Self {
        unsafe {
            return DoubleSolenoid::new(module, module_type, forward_channel, reverse_channel);
        }
    }

    pub fn from_default(
        module_type: PneumaticsModuleType,
        forward_channel: i32,
        reverse_channel: i32,
    ) -> Self {
        unsafe {
            return DoubleSolenoid::new1(module_type, forward_channel, reverse_channel);
        }
    }

    pub fn toggle(&mut self) {
        unsafe {
            self.Toggle();
        }
    }

    pub fn get_fwd_channel(&self) -> i32 {
        unsafe {
            return self.GetFwdChannel();
        }
    }

    pub fn get_rev_channel(&self) -> i32 {
        unsafe {
            return self.GetRevChannel();
        }
    }

    pub fn is_fwd_solenoid_disabled(&self) -> bool {
        unsafe {
            return self.IsFwdSolenoidDisabled();
        }
    }

    pub fn is_rev_solenoid_disabled(&self) -> bool {
        unsafe {
            return self.IsRevSolenoidDisabled();
        }
    }

    pub fn set(&mut self, value: DoubleSolenoidValue) {
        unsafe {
            frc_DoubleSolenoid_Set(self as *mut _ as *mut c_void, value);
        }
    }

    pub fn get(&mut self) -> DoubleSolenoidValue {
        unsafe {
            return frc_DoubleSolenoid_Get(self as *mut _ as *mut c_void);
        }
    }

    pub fn init_sendable(&mut self, builder: &mut SendableBuilder) {
        unsafe {
            frc_DoubleSolenoid_InitSendable(self as *mut _ as *mut c_void, builder);
        }
    }
}
