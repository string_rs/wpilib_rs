use crate::*;

pub type CANStatus = frc_CANStatus;
pub type RobotController = frc_RobotController;
impl RobotController {
    pub fn get_fpga_version() -> i32 {
        unsafe {
            return RobotController::GetFPGAVersion();
        }
    }

    pub fn get_fpga_revision() -> i64 {
        unsafe {
            return RobotController::GetFPGARevision();
        }
    }

    pub fn get_fpga_time() -> u64 {
        unsafe {
            return RobotController::GetFPGATime();
        }
    }

    pub fn get_user_button() -> bool {
        unsafe {
            return RobotController::GetUserButton();
        }
    }

    pub fn get_battery_voltage() -> units_voltage_volt_t {
        unsafe {
            return RobotController::GetBatteryVoltage();
        }
    }

    pub fn is_sys_active() -> bool {
        unsafe {
            return RobotController::IsSysActive();
        }
    }

    pub fn is_browned_out() -> bool {
        unsafe {
            return RobotController::IsBrownedOut();
        }
    }

    pub fn get_input_voltage() -> f64 {
        unsafe {
            return RobotController::GetInputVoltage();
        }
    }

    pub fn get_input_current() -> f64 {
        unsafe {
            return RobotController::GetInputCurrent();
        }
    }

    pub fn get_voltage_3v3() -> f64 {
        unsafe {
            return RobotController::GetVoltage3V3();
        }
    }

    pub fn get_current_3v3() -> f64 {
        unsafe {
            return RobotController::GetCurrent3V3();
        }
    }

    pub fn get_enabled_3v3() -> bool {
        unsafe {
            return RobotController::GetEnabled3V3();
        }
    }

    pub fn get_fault_count_3v3() -> i32 {
        unsafe {
            return RobotController::GetFaultCount3V3();
        }
    }

    pub fn get_voltage_5v() -> f64 {
        unsafe {
            return RobotController::GetVoltage5V();
        }
    }

    pub fn get_current_5v() -> f64 {
        unsafe {
            return RobotController::GetCurrent5V();
        }
    }

    pub fn get_enabled_5v() -> bool {
        unsafe {
            return RobotController::GetEnabled5V();
        }
    }

    pub fn get_fault_count_5v() -> i32 {
        unsafe {
            return RobotController::GetFaultCount5V();
        }
    }

    pub fn get_voltage_6v() -> f64 {
        unsafe {
            return RobotController::GetVoltage6V();
        }
    }

    pub fn get_current_6v() -> f64 {
        unsafe {
            return RobotController::GetCurrent6V();
        }
    }

    pub fn get_enabled_6v() -> bool {
        unsafe {
            return RobotController::GetEnabled6V();
        }
    }

    pub fn get_fault_count_6v() -> i32 {
        unsafe {
            return RobotController::GetFaultCount6V();
        }
    }

    pub fn get_brownout_voltage() -> units_voltage_volt_t {
        unsafe {
            return RobotController::GetBrownoutVoltage();
        }
    }

    pub fn set_brownout_voltage(voltage: units_voltage_volt_t) {
        unsafe {
            RobotController::SetBrownoutVoltage(voltage);
        }
    }

    pub fn get_can_status() -> CANStatus {
        unsafe {
            return RobotController::GetCANStatus();
        }
    }
}
