use crate::*;

pub type MotorSafety = frc_MotorSafety;
impl MotorSafety {
    pub fn from() -> Self {
        unsafe {
            return MotorSafety::new();
        }
    }

    pub fn from_rhs(rhs: &mut Self) -> Self {
        unsafe {
            return MotorSafety::new1(rhs);
        }
    }

    pub fn feed(&mut self) {
        unsafe {
            self.Feed();
        }
    }

    pub fn set_expiration(&mut self, expiration_time: units_time_second_t) {
        unsafe {
            self.SetExpiration(expiration_time);
        }
    }

    pub fn get_expiration(&self) -> units_time_second_t {
        unsafe {
            return self.GetExpiration();
        }
    }

    pub fn is_alive(&self) -> bool {
        unsafe {
            return self.IsAlive();
        }
    }

    pub fn set_safety_enabled(&mut self, enabled: bool) {
        unsafe {
            self.SetSafetyEnabled(enabled);
        }
    }

    pub fn is_safety_enabled(&self) -> bool {
        unsafe {
            return self.IsSafetyEnabled();
        }
    }

    pub fn check(&mut self) {
        unsafe {
            self.Check();
        }
    }

    pub fn check_motors() {
        unsafe {
            MotorSafety::CheckMotors();
        }
    }
}
