# Project isn't actively being developed right now

Turns out I did a lot of things wrong here, and it won't be ready for this build season. I will refactor and begin development again after this build season, in the hope I can get it working for the 2023 season.

# TODO (Good targets for PR if you want to help):

- Refactor using `cxx-enable-namespaces`

- Add rustdoc comments to functions

- Conversion from Rust strings <-> C++ strings (any functions that return or accept strings currently don't work)

- Better implementation for WPILIB's units (such as `units::volt_t`) rather than just using bindgen's bindings for them

# Setup:

1. Download the [FRC toolchain](https://github.com/wpilibsuite/roborio-toolchain/releases) and extract the `roborio` folder within it to `~/.roborio` (so `~/.roborio` includes the `bin`, `include`, `lib`, etc. folders).

2. `git clone --recursive https://gitlab.com/string_rs/wpilib_rs.git`

3. `rustup target add arm-unknown-linux-gnueabi`

    ### Windows:

    Also run these commands (not required, but give faster build times):

    ```cmd
    cargo install -f cargo-binutils
    rustup component add llvm-tools-preview
    ```

4. `cargo build` should successfully build the project.
